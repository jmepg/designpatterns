package Entity.Item.Enemy;

import Entity.Animation;
import TileMap.TileMap;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

public class Boss extends Enemy {
    private BufferedImage[] sprites;


    public Boss(TileMap tm){
        super(tm);

        moveSpeed = 1;
        maxSpeed = 1;
        stopSpeed = 0;
        fallSpeed = 0.15;
        maxFallSpeed = 4.0;
        jumpStart = -5.8;
        stopJumpSpeed = 0.3;

        width = 30;
        height = 30;
        cwidth = 20;
        cheight = 20;

        maxHealth = 20;
        health = maxHealth;
        damage = 5;
        jumping = true;


        //load sprites

        try{
            BufferedImage spritesheet = ImageIO.read(getClass().getResourceAsStream("/Sprites/Enemies/arachnik.gif"));
            sprites = new BufferedImage[1];

            for(int i= 0; i< sprites.length;i++){
                sprites[i] = spritesheet.getSubimage(i*width,0,width,height);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        animation = new Animation();
        animation.setFrames(sprites);
        animation.setDelay(300);

        right = true;
        facingRight = true;
    }


    public void update(){
        getNextPosition();
        checkTileMapCollision();
        setPosition(xtemp,ytemp);


        if(jumping && !falling) {
            dy = jumpStart;
            falling = true;
        }

        //if it hits a wall go other direction
        if(right && dx == 0){
            right = false;
            left = true;
            facingRight = false;
        }
        else if(left && dx == 0){
            right = true;
            left = false;
            facingRight = true;
        }

        //update animation
        animation.update();

    }

}
