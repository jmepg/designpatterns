package Entity.Item.Enemy;

import Entity.Item.MapObject;
import TileMap.TileMap;

import java.awt.*;

public abstract class Enemy extends MapObject implements IEnemy {

    protected int health;
    protected int maxHealth;
    protected boolean dead;
    protected int damage;

    protected boolean flinching;
    protected long flinchTimer;

    public Enemy(TileMap tm){
        super(tm);
    }


    public void setTileMap(TileMap tileMap) {
        super.setTileMap(tileMap);
    }

    public void setTileSize(int tileSize) {
        super.setTileSize(tileSize);
    }

    public boolean isDead(){
        return dead;
    }

    public int getDamage() {
        return damage;
    }

    public int getHealth() {
        return health;
    }

    public void hit(int damage){
        if(flinching){
            long elapsed = (System.nanoTime() - flinchTimer)/1000000;
            if(elapsed > 1000)
                flinching = false;
        }

        if(dead || flinching)
            return;
        health -= damage;
        if(health <= 0)
            dead = true;
        flinching = true;

        flinchTimer = System.nanoTime();
    }

    public void getNextPosition(){
        // movement
        if(left) {
            dx -= moveSpeed;
            if(dx < -maxSpeed) {
                dx = -maxSpeed;
            }
        }
        else if(right) {
            dx += moveSpeed;
            if(dx > maxSpeed) {
                dx = maxSpeed;
            }
        }

        if(falling){
            dy += fallSpeed;
        }
    }

    public void update(){}

    public void draw(Graphics2D g){
       /* if(notOnScreen())
            return;*/
        setMapPosition();
        super.draw(g);
    }

}


