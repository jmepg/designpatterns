package Entity.Item.Enemy;

import Entity.Animation;
import TileMap.TileMap;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

public class Slugger extends Enemy{

    private BufferedImage[] sprites;

    public Slugger(TileMap tm){
        super(tm);

        moveSpeed = 0.3;
        maxSpeed = 0.3;
        fallSpeed = 0.2;

        width = 30;
        height = 30;
        cwidth = 20;
        cheight = 20;

        maxHealth = 2;
        health = maxHealth;
        damage = 1;
        jumping = false;

        //load sprites

        try{
            BufferedImage spritesheet = ImageIO.read(getClass().getResourceAsStream("/Sprites/Enemies/slugger.gif"));
            sprites = new BufferedImage[3];

            for(int i= 0; i< sprites.length;i++){
                sprites[i] = spritesheet.getSubimage(i*width,0,width,height);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        animation = new Animation();
        animation.setFrames(sprites);
        animation.setDelay(300);

        right = true;
        facingRight = true;
    }


    public void update(){
        getNextPosition();
        checkTileMapCollision();
        setPosition(xtemp,ytemp);

        if(falling){
            long elapsed = (System.nanoTime() - flinchTimer)/ 1000000;
            if(elapsed > 400)
                flinching = false;
        }

        //if it hits a wall go other direction
        if(right && dx == 0){
            right = false;
            left = true;
            facingRight = false;
        }
        else if(left && dx == 0){
            right = true;
            left = false;
            facingRight = true;
        }

        //update animation

        animation.update();

    }

}
