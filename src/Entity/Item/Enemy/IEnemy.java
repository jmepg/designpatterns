package Entity.Item.Enemy;

import java.awt.*;

public interface IEnemy {

    void getNextPosition();
    void update();
    void draw(Graphics2D g);

}
