package Entity.Item.Enemy;

import TileMap.TileMap;

import java.util.Hashtable;


public class EnemyFactory {


    public enum EnemyType{
        SLUGGER,
        BOSS
    }

    public static Enemy getEnemy(TileMap tm, EnemyType type){

        switch(type) {
            case SLUGGER:
                return new Slugger(tm);
            case BOSS:
                return new Boss(tm);
            default:
                return null;

        }
    }

}
