package Entity.Item;


import Entity.Item.Enemy.*;
import TileMap.TileMap;
import java.awt.*;

public class ObjectBuilder {


    public Objects createLvl1Obj(TileMap tileMap){


        Objects obj = new Objects();

        Player player = new Player(tileMap);

        obj.addItem(player);

        Point[] points = new Point[]{
                new Point(200,100),
                new Point(860,200),
                new Point(1525,200),
                new Point(1680,200),
                new Point(1800,200)
        };

        for(int i=0; i< points.length;i++){
            createEnemy(tileMap, EnemyFactory.EnemyType.SLUGGER, points[i].x, points[i].y, obj);
        }

        createEnemy(tileMap, EnemyFactory.EnemyType.BOSS, 3100, 200, obj);

        return obj;
    }

    public void createEnemy(TileMap tileMap, EnemyFactory.EnemyType enemyType, double x, double y, Objects obj) {
        Enemy e = EnemyFactory.getEnemy(tileMap, enemyType);
        e.setPosition(x, y);
        obj.addItem(e);
    }

}



