package Entity.Item;

import Entity.Animation;
import Main.GamePanel;
import TileMap.*;
import java.awt.*;
import java.awt.geom.Point2D;

public abstract class MapObject implements Item {

    protected  TileMap tileMap;
    protected int tileSize;
    protected double xmap, ymap;

    protected Point2D.Double point;

    protected double dx, dy; //position and vector

    protected int width, height; //dimensions
    protected int cwidth,cheight; //collision box

    protected int currRow, currCol;
    protected double xdest, ydest;
    protected double xtemp, ytemp;
    protected boolean topLeft, topRight, bottomLeft, bottomRight;

    //animation
    protected Animation animation;
    protected  int currentAction;
    protected boolean facingRight;

    //movement
    protected boolean left, right, up, down, jumping, falling;

    //movement attributes
    protected double moveSpeed, maxSpeed, stopSpeed, fallSpeed, maxFallSpeed;
    protected double jumpStart, stopJumpSpeed;


    public MapObject(TileMap tm){
        tileMap = tm;
        tileSize = tm.getTileSize();
        point = new Point2D.Double();

    }


    public boolean intersects(MapObject obj){
        Rectangle r1 = getRectangle();
        Rectangle r2 = obj.getRectangle();

        return r1.intersects(r2);
    }

    public Rectangle getRectangle(){
        return new Rectangle((int) point.getX() - cwidth, (int) point.getY() - cheight, cwidth, cheight);
    }

    public void calculateCorners(double x, double y) {
        int leftTile = (int)(x - cwidth / 2) / tileSize;
        int rightTile = (int)(x + cwidth / 2 - 1) / tileSize;
        int topTile = (int)(y - cheight / 2) / tileSize;
        int bottomTile = (int)(y + cheight / 2 - 1) / tileSize;
        if(topTile < 0 || bottomTile >= tileMap.getNumRows() ||
                leftTile < 0 || rightTile >= tileMap.getNumCols()) {
            topLeft = topRight = bottomLeft = bottomRight = false;
            return;
        }
        int tl = tileMap.getType(topTile, leftTile);
        int tr = tileMap.getType(topTile, rightTile);
        int bl = tileMap.getType(bottomTile, leftTile);
        int br = tileMap.getType(bottomTile, rightTile);
        topLeft = tl == Tile.BLOCKED;
        topRight = tr == Tile.BLOCKED;
        bottomLeft = bl == Tile.BLOCKED;
        bottomRight = br == Tile.BLOCKED;
    }

    public void checkTileMapCollision(){
        currCol = (int) point.getX() / tileSize;
        currRow = (int)point.getY() / tileSize;

        xdest = point.getX() + dx;
        ydest = point.getY() + dy;


        xtemp = point.getX();
        ytemp = point.getY();

        calculateCorners(point.getX(), ydest);

        if(dy < 0){
            if(topLeft || topRight){
                dy = 0;
                ytemp = currRow * tileSize + cheight / 2;
            }
            else{
                ytemp += dy;
            }
        }
        if(dy > 0){
            if(bottomLeft || bottomRight){
                dy = 0;
                falling = false;
                ytemp = (currRow + 1) * tileSize - cheight / 2;
            }
            else{
                ytemp += dy;
            }
        }

        calculateCorners(xdest, point.getY());

        if(dx < 0){
            if(topLeft || bottomLeft){
                dx = 0;
                xtemp = currCol * tileSize + cwidth / 2;
            }
            else{
                xtemp += dx;
            }
        }
        if(dx > 0){
            if(topRight || bottomRight){
                dx = 0;
                xtemp = (currCol + 1) * tileSize - cwidth / 2;
            }
            else{
                xtemp += dx;
            }
        }

        if(!falling){
            calculateCorners(point.getX(), ydest + 1);
            if(!bottomLeft && ! bottomRight){
                falling = true;
            }
        }

    }

    public double getX() {
        return point.getX();
    }

    public double getY() {
        return point.getY();
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getCwidth() {
        return cwidth;
    }

    public int getCheight() {
        return cheight;
    }

    public void setTileMap(TileMap tileMap) {
        this.tileMap = tileMap;
    }

    public void setTileSize(int tileSize) {
        this.tileSize = tileSize;
    }

    public void setPosition(double x, double y){
        this.point.setLocation(x,y);
    }

    public void setVector(double dx, double dy){
        this.dx = dx;
        this.dy = dy;
    }

    public void setMapPosition(){
        xmap = tileMap.getx();
        ymap = tileMap.gety();
    }

    public void setLeft(boolean left) {
        this.left = left;
    }

    public void setRight(boolean right) {
        this.right = right;
    }

    public void setUp(boolean up) {
        this.up = up;
    }

    public void setDown(boolean down) {
        this.down = down;
    }

    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

    public boolean notOnScreen(){
        return point.getX() + xmap + width < 0 ||
                point.getX() + xmap - width > GamePanel.WIDTH ||
                point.getY() + ymap + height < 0 ||
                point.getY() + ymap - height > GamePanel.HEIGHT;
    }

    public void draw(Graphics2D g){
        if(facingRight) {
            g.drawImage(
                    animation.getImage(),
                    (int)(point.getX() + xmap - width / 2),
                    (int)(point.getY() + ymap - height / 2),
                    null
            );
        }
        else {
            g.drawImage(
                    animation.getImage(),
                    (int)(point.getX() + xmap - width / 2 + width),
                    (int)(point.getY() + ymap - height / 2),
                    -width,
                    height,
                    null
            );

        }
    }


}
