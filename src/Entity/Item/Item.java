package Entity.Item;

import java.awt.*;

public interface Item {

    Rectangle getRectangle();
    void calculateCorners(double x, double y);
    void checkTileMapCollision();
    boolean intersects(MapObject obj);
    boolean notOnScreen();
}
