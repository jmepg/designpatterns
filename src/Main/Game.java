package Main;

import javax.swing.JFrame;

public class Game {

    public static void main(String[] args){
        JFrame window = new JFrame("Space Conquest");
        window.setContentPane(GamePanel.gamePanel);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setResizable(false);
        window.pack();
        window.setVisible(true);
    }


}
