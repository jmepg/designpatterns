package GameState;


import java.util.List;
import java.util.ArrayList;

public class GameStateManager {
    private List<GameState> gameStates;
    private int currentState;

    private MenuState menu;
    private Level1State lvl1;

    public static final int MENUSTATE = 0;
    public static final int LEVEL1STATE = 1;


    public static final GameStateManager gsm = new GameStateManager();

    private GameStateManager(){
        
        lvl1 = new Level1State();
        menu = new MenuState();

        gameStates = new  ArrayList<GameState>();
        gameStates.add(menu);
        gameStates.add(lvl1);
    }

    public void setState(int state){
        currentState = state;
        gameStates.get(currentState).init();
    }

    public void update(){
        gameStates.get(currentState).update();
    }

    public void draw(java.awt.Graphics2D g){
        gameStates.get(currentState).draw(g);
    }


    public int getCurrentState() {
        return currentState;
    }

    public Level1State getLvl1() {
        return lvl1;
    }
}
