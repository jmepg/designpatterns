package GameState;

import Entity.*;
import Entity.Item.Enemy.Enemy;
import Entity.Item.ObjectBuilder;
import Entity.Item.Objects;
import Entity.Item.Player;
import Main.GamePanel;
import TileMap.*;

import java.awt.*;
import java.util.List;
import java.util.ArrayList;


import static GameState.GameStateManager.MENUSTATE;

public class Level1State implements GameState {

    private TileMap tileMap;
    private Background bg;

    private Player player;

    private List<Enemy> enemies;
    public List<Explosion> explosions;

    private HUD hud;

    ObjectBuilder objectBuilder = new ObjectBuilder();


    public Level1State() {
        init();
    }

    public void init() {

        for(int i =0; i < CommandManager.instance.getCommandsSize();i++){
            if(i>=3){
                CommandManager.instance.enableCommand(i);
        }
            else
                CommandManager.instance.disableCommand(i);
        }

        tileMap = new TileMap(30);
        tileMap.loadTiles("/Tilesets/grasstileset.gif");
        tileMap.loadMap("/Maps/level1-1.map");
        tileMap.setPosition(0, 0);
        tileMap.setTween(0.07);

        bg = new Background("/Backgrounds/grassbg1.gif", 0.1);

        enemies = new ArrayList<Enemy>();

        Objects obj = objectBuilder.createLvl1Obj(tileMap);

        for(int i = 0; i<obj.getItems().size();i++){
            if(obj.getItems().get(i) instanceof Player){
                player = (Player) obj.getItems().get(i);
            }
            else if(obj.getItems().get(i) instanceof Enemy){
                enemies.add((Enemy) obj.getItems().get(i));
            }
        }


        player.setPosition(100,100);


        explosions = new ArrayList<Explosion>();

        hud = new HUD(player);
    }




    public void update() {
        player.update();

        if(player.isDead()){
            long elapsed = (System.nanoTime() - player.getDeadTime())/1000000;
            if(elapsed > 5000){
            gsm.setState(MENUSTATE);

            }
            return;
        }
        tileMap.setPosition(GamePanel.WIDTH/2 - player.getX(), GamePanel.HEIGHT/2 - player.getY());



        player.checkAttack(enemies);

        for(int i =0; i< enemies.size();i++){
            Enemy e = enemies.get(i);
            e.update();
            if(e.isDead()){
                enemies.remove(i);
                i--;
                explosions.add(new Explosion((int) e.getX(),(int) e.getY()));

            }
        }


        for(int i=0;i<explosions.size();i++){
            explosions.get(i).update();
            if(explosions.get(i).shouldRemove()){
                explosions.remove(i);
                i--;
            }
        }
    }

    public void draw(Graphics2D g) {

        //draw bg
        bg.draw(g);

        // draw tilemap
        tileMap.draw(g);

        if(player.isDead()){
            Font font = new Font("Arial", Font.BOLD, 20);
            g.setFont(font);
            g.setColor(Color.RED);
            g.drawString("GAME OVER",100,50);

            return;
        }

        //draw player
        player.draw(g);

        //draw enemies
        for(int i =0;i<enemies.size();i++){
            enemies.get(i).draw(g);
        }

        //draw explosions
        for(int i=0;i<explosions.size();i++){
            explosions.get(i).setMapPosition((int) tileMap.getx(), (int) tileMap.gety());
            explosions.get(i).draw(g);
        }

        //draw HUD
        hud.draw(g);

    }

    public Player getPlayer() {
        return player;
    }
}












