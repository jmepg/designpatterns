package GameState;

import java.awt.event.KeyEvent;

public class CommandGliding extends IStateCommand.StateCommand{


    public CommandGliding(){super(KeyEvent.VK_E);}

    protected void doExecute(){
        gsm.getLvl1().getPlayer().setGliding(true);
    }

    @Override
    protected void stopExecute() {
        gsm.getLvl1().getPlayer().setGliding(false);
    }
}
