package GameState;

import TileMap.Background;

import java.awt.*;
import java.awt.event.KeyEvent;

import static GameState.CommandMenu.currentChoice;
import static GameState.CommandMenu.options;


public class MenuState implements GameState {

    private Background bg;

    private  Color titleColor;
    private  Font titleFont;
    private  Font font;


    public MenuState(){
        init();
        try{
            bg = new Background("/Backgrounds/menubg.gif", 1);
            bg.setVector(-0.1, 0);

            titleColor = new Color(128,0,0);
            titleFont = new Font("Century Gothic", Font.PLAIN, 28);
            font = new Font("Arial", Font.PLAIN, 12);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void init(){
        System.out.println("MENUSTATE");

        for(int i =0; i < CommandManager.instance.getCommandsSize();i++){
            if(i<3){
                System.out.println("i:" +i);
                CommandManager.instance.enableCommand(i);
            }
            else
                CommandManager.instance.disableCommand(i);
        }

    }

    public void update(){
        //bg.update();
    }

    public void draw (Graphics2D g){
        bg.draw(g);

        g.setColor(Color.GRAY);
        g.setFont(titleFont);
        g.drawString("Space Conquest", 10,50);

        g.setFont(font);

        for(int i=0; i<options.length;i++){
            if(i==currentChoice)
                g.setColor(Color.WHITE);
            else
                g.setColor(Color.GRAY);
            g.drawString(options[i], 145,140 + i*15);

        }
    }


}
