package GameState;

import java.awt.event.KeyEvent;

public class CommandEnter extends IStateCommand.StateCommand{


    public CommandEnter(){super(KeyEvent.VK_ENTER);}

    protected void doExecute(){
        CommandMenu.select();
    }

    @Override
    protected void stopExecute() {
    }
}