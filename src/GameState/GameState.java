package GameState;

public interface GameState {
    GameStateManager gsm = GameStateManager.gsm;

    void init();
    void update();
    void draw(java.awt.Graphics2D g);

}
