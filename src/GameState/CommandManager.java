package GameState;

import java.util.HashMap;

public class CommandManager {

    public static CommandManager instance = new CommandManager();

    private HashMap<Integer, IStateCommand.StateCommand> commands;

    private CommandManager(){
        this.commands = new HashMap<>();

        this.commands.put(0, new CommandEnter());
        this.commands.put(1, new CommandMenuUp());
        this.commands.put(2, new CommandMenuDown());
        this.commands.put(3, new CommandJump());
        this.commands.put(4, new CommandFiring());
        this.commands.put(5, new CommandGliding());
        this.commands.put(6, new CommandScratching());
        this.commands.put(7, new CommandLeft());
        this.commands.put(8, new CommandRight());
        this.commands.put(9, new CommandPlayerUp());
        this.commands.put(10, new CommandPlayerDown());
    }

    public void enableCommand(int id) {
        if (this.commands.containsKey(id)) {
            this.commands.get(id).enable();
        }
    }

    public void disableCommand(int id) {
        if (this.commands.containsKey(id)) {
            this.commands.get(id).disable();
        }
    }

    public int getCommandsSize() {
        return commands.size();
    }

    public IStateCommand.StateCommand getCommand(int i){
        return this.commands.get(i);
    }
}
