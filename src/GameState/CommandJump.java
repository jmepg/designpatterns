package GameState;

import java.awt.event.KeyEvent;

public class CommandJump extends IStateCommand.StateCommand{


    public CommandJump(){super(KeyEvent.VK_W);}

    protected void doExecute(){

        GameStateManager.gsm.getLvl1().getPlayer().setJumping(true);
    }

    @Override
    protected void stopExecute() {
        gsm.getLvl1().getPlayer().setJumping(false);
    }
}
