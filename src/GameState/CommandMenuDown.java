package GameState;

import java.awt.event.KeyEvent;

public class CommandMenuDown extends IStateCommand.StateCommand{


    public CommandMenuDown(){super(KeyEvent.VK_DOWN);}

    protected void doExecute(){
        CommandMenu.currentChoice++;
        if(CommandMenu.currentChoice == CommandMenu.options.length)
            CommandMenu.currentChoice = 0;
    }

    @Override
    protected void stopExecute() {
    }
}