package GameState;

import java.awt.event.KeyEvent;

public class CommandMenuUp extends IStateCommand.StateCommand{


    public CommandMenuUp(){super(KeyEvent.VK_UP);}

    protected void doExecute(){
        CommandMenu.currentChoice--;
        if(CommandMenu.currentChoice == -1)
            CommandMenu.currentChoice = CommandMenu.options.length - 1;
    }

    @Override
    protected void stopExecute() {
    }
}