package GameState;

import java.awt.event.KeyEvent;

public class CommandPlayerUp extends IStateCommand.StateCommand{


    public CommandPlayerUp(){super(KeyEvent.VK_UP);}

    protected void doExecute(){

        GameStateManager.gsm.getLvl1().getPlayer().setUp(true);
    }

    @Override
    protected void stopExecute() {
        gsm.getLvl1().getPlayer().setUp(false);
    }
}
