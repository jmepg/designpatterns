package GameState;

import java.awt.event.KeyEvent;

public class CommandFiring extends IStateCommand.StateCommand{


    public CommandFiring(){super(KeyEvent.VK_F);}

    protected void doExecute(){
        gsm.getLvl1().getPlayer().setFiring();
    }

    @Override
    protected void stopExecute() {

    }
}
