package GameState;

import java.awt.event.KeyEvent;

public class CommandScratching extends IStateCommand.StateCommand{


    public CommandScratching(){super(KeyEvent.VK_R);}

    protected void doExecute(){

        GameStateManager.gsm.getLvl1().getPlayer().setScratching();
    }

    @Override
    protected void stopExecute() {

    }
}
