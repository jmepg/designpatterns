package GameState;

import java.awt.event.KeyEvent;

public class CommandLeft extends IStateCommand.StateCommand{


    public CommandLeft(){super(KeyEvent.VK_LEFT);}

    protected void doExecute(){
        GameStateManager.gsm.getLvl1().getPlayer().setLeft(true);
    }

    @Override
    protected void stopExecute() {
        gsm.getLvl1().getPlayer().setLeft(false);
    }
}
