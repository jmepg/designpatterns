package GameState;

import java.awt.event.KeyEvent;

public class CommandPlayerDown extends IStateCommand.StateCommand{


    public CommandPlayerDown(){super(KeyEvent.VK_DOWN);}

    protected void doExecute(){
        GameStateManager.gsm.getLvl1().getPlayer().setDown(true);
    }

    @Override
    protected void stopExecute() {
        gsm.getLvl1().getPlayer().setDown(false);
    }
}
