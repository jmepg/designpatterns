package GameState;

import java.awt.event.KeyEvent;

public class CommandRight extends IStateCommand.StateCommand{


    public CommandRight(){super(KeyEvent.VK_RIGHT);}

    protected void doExecute(){

        GameStateManager.gsm.getLvl1().getPlayer().setRight(true);
    }

    @Override
    protected void stopExecute() {
        gsm.getLvl1().getPlayer().setRight(false);
    }
}
