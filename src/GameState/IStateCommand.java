package GameState;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public interface IStateCommand {

    GameStateManager gsm = GameStateManager.gsm;

    void enable();
    void disable();

    abstract class StateCommand implements IStateCommand, KeyListener{

        private boolean enabled;
        private int keyCode;

         protected abstract void doExecute();

         protected abstract void stopExecute();

         public StateCommand(int keyCode){
            super();
            this.enabled = false;
            this.keyCode = keyCode;
        }

        public final void execute() {
            if (this.enabled) {
                this.doExecute();
            }
        }

        public final void notExecute(){
             if(this.enabled){
                 this.stopExecute();
             }
        }

        @Override
        public void enable() {
            this.enabled = true;
        }

        @Override
        public void disable() {
            this.enabled = false;
        }

        public boolean isEnabled() {
            return enabled;
        }


        public void keyTyped(KeyEvent key) {
            keyPressed(key);
        }

        public void keyPressed(KeyEvent key) {
            if (key.getKeyCode() == this.keyCode)
                this.execute();
        }

        public void keyReleased(KeyEvent key) {
            if (key.getKeyCode() == this.keyCode)
                this.notExecute();

        }

    }





}

